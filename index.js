'use strict';
// https://getindiekit.com/docs/getting-started/
import {Indiekit} from '@indiekit/indiekit';
import {PelicanPreset} from './preset_pelican.js';
import {GiteaStore} from '@indiekit/store-gitea';

// Create a new indiekit instance
const indiekit = new Indiekit();

// Configure publication
indiekit.set('publication.me', 'https://fluid.quest');

// Add a publication preset
const pelican = new PelicanPreset();
indiekit.set('publication.preset', pelican);

// Add a content store
const gitea = new GiteaStore({
  instance: 'https://codeberg.org',
  user: 'ashwinvis', // Your username on Gitea
  repo: 'website', // Repository files will be saved to
  branch: 'main', // Branch to publish to
  token: process.env.GITEA_TOKEN, // Gitea personal access token
});
indiekit.set('publication.store', gitea);


indiekit.set('publication.timeZone', 'Europe/Stockholm');


// Create a server
const server = indiekit.server();

// Export server
export default server;
