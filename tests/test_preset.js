import test from 'ava';
import PelicanPreset from '../preset_pelican.js';
import {readFileSync} from 'fs';
//
// const test = require('ava').test;
// const PelicanPreset = require('../preset_pelican.js').PelicanPreset;

const pelican = new PelicanPreset();

test.beforeEach((t) => {
  const properties = readFileSync('all-properties.jf2', 'utf8');
  t.context.properties = JSON.parse(properties);
});

test('Gets publication post types', (t) => {
  const result = pelican.postTypes;

  t.is(result[0].type, 'article');
});

test('Renders post template without title', (t) => {
  const result = pelican.postTemplate({
    published: '2020-02-02',
    type: 'note',
  });

  t.is(result, `---
Category: Microblog
Date: 2020-02-02
Title: 🗒️
---
`);
});

test('Renders post template without content', (t) => {
  const result = pelican.postTemplate({
    published: '2020-02-02',
    name: 'What I had for lunch',
  });

  t.is(result, `---
Category: Microblog
Date: 2020-02-02
Title: What I had for lunch
---
`);
});

test('Renders post template with basic content', (t) => {
  const result = pelican.postTemplate({
    published: '2020-02-02',
    name: 'What I had for lunch',
    content: 'I ate a [cheese](https://en.wikipedia.org/wiki/Cheese) sandwich, which was nice.',
  });

  t.is(result, `---
Category: Microblog
Date: 2020-02-02
Title: What I had for lunch
---
I ate a [cheese](https://en.wikipedia.org/wiki/Cheese) sandwich, which was nice.
`);
});

test('Renders post template with HTML content', (t) => {
  const result = pelican.postTemplate({
    published: '2020-02-02',
    name: 'What I had for lunch',
    content: {
      html: '<p>I ate a <a href="https://en.wikipedia.org/wiki/Cheese">cheese</a> sandwich, which was nice.</p>',
    },
  });

  t.is(result, `---
Category: Microblog
Date: 2020-02-02
Title: What I had for lunch
---
<p>I ate a <a href="https://en.wikipedia.org/wiki/Cheese">cheese</a> sandwich, which was nice.</p>
`);
});


test('Renders post template with YAML frontmatter', (t) => {
  const pelican = new PelicanPreset({frontMatterFormat: 'yaml'});

  const result = pelican.postTemplate(t.context.properties);

  t.is(result, `---
Category: Microblog
Date: 2020-02-02
Title: What I had for lunch
Summary: A very satisfactory meal.
Tags: lunch, food
Start: 2020-02-02
End: 2020-02-20
Rsvp: Yes
Location:
    Type: adr
    CountryName: United Kingdom
Checkin:
    Type: card
    Latitude: "50"
    Longitude: "0"
Audio:
    - Url: https://website.example/audio.mp3
Images:
    - Alt: Alternative text
      Url: https://website.example/photo.jpg
Videos:
    - Url: https://website.example/video.mp4
BookmarkOf: https://website.example
LikeOf: https://website.example
RepostOf: https://website.example
InReplyTo: https://website.example
Status: draft
Visibility: private
Syndication: https://website.example/post/12345
Slug: cheese-sandwich
MpSyndicateTo: https://social.example
---
I ate a [cheese](https://en.wikipedia.org/wiki/Cheese) sandwich, which was nice.
`);
});
