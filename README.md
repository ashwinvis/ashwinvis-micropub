# ashwinvis-micropub
Configuration for micropub server using indiekit


## Debug

    yarn start


## Tests

    yarn test


## Deploy

### First time

- Heroku: <https://heroku.com/deploy?template=https://codeberg.org/ashwinvis/ashwinvis-micropub/src/branch/main>

### Command line

- Using Git and Heroku CLI

```bash
git clone git@codeberg.org:ashwinvis/ashwinvis-micropub
cd ashwinvis-micropub

# setup heroku
npm install -g heroku
heroku login
heroku git:remote -a ashwinvis-micropub

# make changes
yarn install
yarn version

# deploy
git push origin main
git push heroku main
```

## See also

This server was made possible thanks to:

- [Indiekit project](https://getindiekit.com/)
- [Paul's Indiekit repo](https://github.com/paulrobertlloyd/paulrobertlloyd-indiekit)

Feel free to fork it. You should start by modifying `index.js` to your liking.
