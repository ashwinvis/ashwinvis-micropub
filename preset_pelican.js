/* Indiekit preset for Pelican static site generator
 *
 * Derived from @indiekit/preset-hugo
 *
MIT License

Copyright (c) 2019 Paul Robert Lloyd

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

import camelcaseKeys from 'camelcase-keys';
import YAML from 'yaml';

// YAML options
// YAML.scalarOptions.str.fold.lineWidth = 0;

const defaults = {
  frontMatterFormat: 'yaml',
};

/**
 * Concatenate arrays as comma separated strings
 *
 * @param {string} key
 * @param {object} value
 *
 * @return {object} Replaced value
 */
function yamlReplacer(key, value) {
  if (Array.isArray(value) && typeof value[0] !== 'object') {
    return value.join(', ');
  } else {
    return value;
  };
};


export const PelicanPreset = class {
  /**
   * Class constructor
   *
   * @param {object} options User customizations
   */
  constructor(options = {}) {
    this.id = 'pelican';
    this.name = 'Pelican';
    this.category = 'Microblog';
    this.options = {...defaults, ...options};
  }

  /**
   * Get front matter
   *
   * @private
   * @param {object} properties Post data variables
   * @return {string} Front matter in chosen format
   */
  _getFrontMatter(properties) {
    let delimiters;
    let frontmatter;

    switch (this.options.frontMatterFormat) {
      case 'json':
        delimiters = ['', '\n'];
        frontmatter = JSON.stringify(properties, null, 2);
        break;
      case 'yaml':
      default:
        delimiters = ['---\n', '---\n'];
        frontmatter = YAML.stringify(
            properties,
            yamlReplacer,
            {lineWidth: 0, indent: 4},
        );
        break;
    }

    return `${delimiters[0]}${frontmatter}${delimiters[1]}`;
  }

  /**
   * Post types
   *
   * @return {object} Post types config
   */
  get postTypes() {
    return [{
      type: 'article',
      name: 'Article',
      post: {
        path: 'content/articles/{slug}.md',
        url: '{slug}',
      },
      media: {
        path: 'content/media/{yyyy}/{MM}/{filename}',
        url: 'media/{yyyy}/{MM}/{filename}',
      },
    }, {
      type: 'note',
      name: 'Note',
      post: {
        path: 'content/notes/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'photo',
      name: 'Photo',
      post: {
        path: 'content/photos/{slug}.md',
        url: '{slug}',
      },
      media: {
        path: 'content/media/photos/{yyyy}/{MM}/{filename}',
        url: 'photos/{yyyy}/{MM}/{filename}',
      },
    }, {
      type: 'video',
      name: 'Video',
      post: {
        path: 'content/videos/{slug}.md',
        url: '{slug}',
      },
      media: {
        path: 'content/media/videos/{yyyy}/{MM}/{filename}',
        url: 'videos/{yyyy}/{MM}/{filename}',
      },
    }, {
      type: 'audio',
      name: 'Audio',
      post: {
        path: 'content/audio/{slug}.md',
        url: '{slug}',
      },
      media: {
        path: 'content/media/audio/{yyyy}/{MM}/{filename}',
        url: 'audio/{yyyy}/{MM}/{filename}',
      },
    }, {
      type: 'bookmark',
      name: 'Bookmark',
      post: {
        path: 'content/bookmarks/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'checkin',
      name: 'Checkin',
      post: {
        path: 'content/checkins/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'event',
      name: 'Event',
      post: {
        path: 'content/events/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'rsvp',
      name: 'Reply with RSVP',
      post: {
        path: 'content/replies/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'reply',
      name: 'Reply',
      post: {
        path: 'content/replies/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'repost',
      name: 'Repost',
      post: {
        path: 'content/reposts/{slug}.md',
        url: '{slug}',
      },
    }, {
      type: 'like',
      name: 'Like',
      post: {
        path: 'content/likes/{slug}.md',
        url: '{slug}',
      },
    }];
  }

  /**
   * Emoji as replacement for a blank title
   *
   * @param {string} pt Post Type
   *
   * @return {string} emoji
   */
  emojiType(pt) {
    return {
      'article': '📜',
      'note': '🗒️',
      'photo': '📷',
      'video': '📹',
      'audio': '🔉',
      'bookmark': '⭐️',
      'mention': '💬',
      'checkin': '📅',
      'event': '📅',
      'rsvp': '📅',
      'reply': '💬',
      'repost': '🔄',
      'like': '❤️',
    }[pt];
  }

  /**
   * Post template
   *
   * @param {object} properties Post data variables
   * @return {string} Rendered template
   */
  postTemplate(properties) {
    /*
     * Pelican uses PascalCase for its predefined front matter keys
     *
     * https://docs.getpelican.com/en/stable/content.html#file-metadata
     * https://python-markdown.github.io/extensions/meta_data/
     */
    properties = camelcaseKeys(properties, {deep: true, pascalCase: true});

    let content;
    if (properties.Content) {
      content = (
        properties.Content.Text ||
        properties.Content.Html ||
        properties.Content
      );
      content = `${content}\n`;
    } else {
      content = '';
    }

    // Pelican does not process a post with a blank Title, and
    // it Tags are better suited to represent categories
    properties = {
      Category: this.category,
      Date: properties.Published,
      ...(
        properties.Name && {Title: properties.Name} ||
        properties.Type && {Title: this.emojiType(properties.Type)}
      ),
      ...(properties.Summary && {Summary: properties.Summary}),
      ...(properties.Category && {Tags: properties.Category}),
      ...(properties.Start && {Start: properties.Start}),
      ...(properties.End && {End: properties.End}),
      ...(properties.Rsvp && {Rsvp: properties.Rsvp}),
      ...(properties.Location && {Location: properties.Location}),
      ...(properties.Checkin && {Checkin: properties.Checkin}),
      ...(properties.Audio && {Audio: properties.Audio}),
      ...(properties.Photo && {Images: properties.Photo}),
      ...(properties.Video && {Videos: properties.Video}),
      ...(properties.BookmarkOf && {BookmarkOf: properties.BookmarkOf}),
      ...(properties.LikeOf && {LikeOf: properties.LikeOf}),
      ...(properties.RepostOf && {RepostOf: properties.RepostOf}),
      ...(properties.InReplyTo && {InReplyTo: properties.InReplyTo}),
      ...(properties.PostStatus && {Status: properties.PostStatus}),
      ...(properties.Visibility && {Visibility: properties.Visibility}),
      ...(properties.Syndication && {Syndication: properties.Syndication}),
      ...(properties.MpSlug && {Slug: properties.MpSlug}),
      ...(
        properties.MpSyndicateTo &&
        {MpSyndicateTo: properties.MpSyndicateTo}
      ),
    };

    const frontMatter = this._getFrontMatter(properties);

    return frontMatter + content;
  }
};

export default PelicanPreset;
